import { RouterModule, Routes } from '@angular/router';

import { PromodoComponent } from './promodo/promodo.component';


const routes: Routes = [
  { path: '', component: PromodoComponent }
];

export const routing = RouterModule.forRoot(routes);
