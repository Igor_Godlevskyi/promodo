describe('Promodo', function () {

  beforeEach(function () {
    browser.get('/');
  });

  it('should have <pr-widget>', function () {
    var home = element(by.css('pr-app pr-widget'));
    expect(home.isPresent()).toEqual(true);
    expect(home.getText()).toEqual("Hello Promodoro Widget");
  });

});
