import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pr-widget',
  templateUrl: './promodo.component.html',
  styleUrls: ['./promodo.component.scss']
})

export class PromodoComponent implements OnInit {
  readonly sessionLen = 25;
  readonly inactiveText = 'Start';
  readonly activeText = 'Reset';
  private currTimer: number;
  public currTimerDisplay: string;
  public isActive: boolean;
  public discardedPromodos: number;
  public succeededPromodos: number;

  ngOnInit() {
    console.log('Hello Promodoro Widget');
    this.discardedPromodos = 0;
    this.succeededPromodos = 0;
    this.isActive = false;
    this.currTimer = this.sessionLen * 60;
    this.updateStringTimer();
  }

  public get timerStateTaxt(): string {
    return this.isActive ? this.activeText : this.inactiveText;
  }

  toggleActive(): void {
    this.isActive = !this.isActive;
  }

  toggleTimer(): void {
    if (!this.isActive) {
      this.incrementTimer();
    } else {
      this.resetTimer();
      this.discardedPromodos++;
    }
    this.toggleActive();
  }

  incrementTimer(): void {
    setTimeout(() => {
      if (this.currTimer > 0 && this.isActive) {
        this.tick();
        this.incrementTimer();
      } else if (this.currTimer === 0) {
        this.succeededPromodos++;
        this.toggleActive();
        this.resetTimer();
      }
    }, 1000);
  }

  tick(): void {
    this.currTimer--;
    this.updateStringTimer();
  }

  resetTimer(): void {
    this.currTimer = this.sessionLen * 60;
    this.updateStringTimer();
  }

  updateStringTimer(): void {
    let totalSecsLeft = this.currTimer;
    let minutes = Math.floor(totalSecsLeft / 60) + '';
    let seconds = totalSecsLeft % 60 + '';
    if (seconds.length < 2) {
      seconds = '0' + seconds;
    }
    this.currTimerDisplay = `${minutes}:${seconds}`;
  }

  setPlayClasses(): Object {
    return {
      'fa': true,
      'fa-play': !this.isActive,
      'fa-stop-circle': this.isActive
    };
  }
}
