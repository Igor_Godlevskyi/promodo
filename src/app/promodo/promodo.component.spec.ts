// This shows a different way of testing a component, check about for a simpler one
import { Component } from '@angular/core';

import { TestBed } from '@angular/core/testing';

import { PromodoComponent } from './promodo.component';

describe('Promodo widget conponent', () => {
  const html = '<pr-widget></pr-widget>';

  beforeEach(() => {
    TestBed.configureTestingModule({declarations: [PromodoComponent, TestComponent]});
    TestBed.overrideComponent(TestComponent, { set: { template: html }});
  });

  it('should ...', () => {
    const fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    expect(fixture.nativeElement.children[0].textContent).toContain('Hello Promodoro Widget');
  });

});

@Component({selector: 'pr-test', template: ''})
class TestComponent { }
