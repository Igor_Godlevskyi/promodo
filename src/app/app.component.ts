import { Component } from '@angular/core';
import { ApiService } from './shared';

import '../style/app.scss';

@Component({
  selector: 'pr-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  url = 'https://en.wikipedia.org/wiki/Pomodoro_Technique';
  title: string;

  constructor( private api: ApiService ) {
    this.title = this.api.title;
  }
}
